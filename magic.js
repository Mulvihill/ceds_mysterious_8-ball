const responses = ["It is known", "It will not be", "Your questions will be affirmed", "Not the way I see it", "It cannot be denied", "It is ill-fated", "Undoubtedly", "The situation dictates no"]

function eightBall () {
    var x = responses[Math.floor(Math.random() * 8)];
    var responseDiv = document.createElement("div");
    responseDiv.textContent = x;
    var getParent = document.getElementById("response");
    getParent.appendChild(responseDiv);
}